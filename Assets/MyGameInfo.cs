﻿using UnityEngine;

[System.Serializable]
class MyGameInfo {
	public string Name="";
	public int GamePin=0;
	public float Players=0;

	public static MyGameInfo CreateFromJSON(string jsonString)
	{
		return JsonUtility.FromJson<MyGameInfo>(jsonString);
	}

	// Given JSON input:
	// {"name":"Dr Charles","lives":3,"health":0.8}
	// this example will return a PlayerInfo object with
	// name == "Dr Charles", lives == 3, and health == 0.8f.
}

