﻿using UnityEngine;

using System.Collections;

using UnityEngine.Networking;

using System.Collections.Generic;




public class GameStateManager : NetworkBehaviour

{

	//an enum, Player 1 = 1, Player 2 = 2.

	public static int playerTurn = -1;

	public static int turnsPassed = 1;

	public static int deliveryCounter = 0;

	public static ArrayList connectedPlayers = new ArrayList(); 

	public static ArrayList factoryShipments = new ArrayList();
	public static ArrayList distributorShipments = new ArrayList();
	public static ArrayList wholesalerShipments = new ArrayList();
	public static ArrayList retailerShipments = new ArrayList();

	public static int factoryOrder;
	public static int distributorOrder;
	public static int wholesalerOrder;
	public static int retailerOrder;

	public static int backlogCost = 2;
	public static int inventoryCost = 1;

	//The customer demand per turn
	public static int[] demand = new int[40]  { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5};

	public static bool turnUpdated = false;

	//set up
	void Start () {
		int y = 0;
		int x = 5;
		factoryShipments.Add (y);
		factoryShipments.Add (x);
		distributorShipments.Add (y);
		distributorShipments.Add (x);
		wholesalerShipments.Add (y);
		wholesalerShipments.Add (x);
		retailerShipments.Add (y);
		retailerShipments.Add (x);
	}


	//call this every time a player connects

	public static void addPlayer(int networkId)

	{

		connectedPlayers.Add(networkId);

	}




	//call this every time a player disconnects

	public static void removePlayer(int networkId)

	{

		connectedPlayers.Remove(networkId);

	}




	public static int getPlayerTurn()

	{

		if (playerTurn == -1) playerTurn = (int)connectedPlayers[0];

		return playerTurn;

	}




	/*

     * Increment the player's turn

     */

	public static void nextTurn()

	{
		int currentIndex = connectedPlayers.IndexOf(playerTurn);

		if (currentIndex == connectedPlayers.Count - 1) playerTurn = (int)connectedPlayers[0];

		else playerTurn = (int)connectedPlayers[currentIndex + 1];

		if (deliveryCounter > 2) {
			deliveryCounter = 0;
			turnsPassed++;
		} else {
			deliveryCounter++;
		}


	}

	//This delivers a shipment to the current player, called once per turn. Also removes the shipment from the list
	public static int GetDelivery() {

		int temp = 0;
		switch(deliveryCounter)
		{
		case 0:
			temp = (int)factoryShipments [0];
			factoryShipments.RemoveAt (0);
			return temp;
		case 1:
			temp = (int) distributorShipments [0];
			distributorShipments.RemoveAt (0);
			return temp;
		case 2:
			temp = (int) wholesalerShipments [0];
			wholesalerShipments.RemoveAt (0);
			return temp;
		case 3:
			temp = (int) retailerShipments [0];
			retailerShipments.RemoveAt (0);
			return temp;
		default:
			return temp;
		}
	}

	//Adding a delivery, this adds to the list ensuring a 2 round delay for delivires.
	public static void AddDelivery(int delivery) {
		
		switch(deliveryCounter)
		{
		case 0:
			distributorShipments.Add(delivery);
			break;
		case 1:
			wholesalerShipments.Add(delivery);
			break;
		case 2:
			retailerShipments.Add(delivery);
			break;
		case 3:
			//Ugly code, but should get the job done.
			//In other cases we need logic elsewhere to ensure that there is enough units to send, but factory will always get the amount they order. So this is fine
			factoryShipments.Add (factoryOrder);
			break;
		default:
			break;
		}
	}
	//Sends the order to the appropriate authority
	public static void SendOrder(int order) {
		
		switch(deliveryCounter)
		{
		case 0:
			factoryOrder = order;
			break;
		case 1:
			distributorOrder = order;
			break;
		case 2:
			wholesalerOrder = order;
			break;
		case 3:
			retailerOrder = order;
			break;
		default:
			break;
		}
	}
	//Sends the order to the appropriate authority
	public static int RecieveOrder() {

		switch(deliveryCounter)
		{
		case 0:
			return distributorOrder;
		case 1:
			return wholesalerOrder;
		case 2:
			return retailerOrder;
		case 3:
			//return the demand based on what turn it is
			return demand [turnsPassed];
		default:
			return 0;
		}
	}

	//Check how many turns into the game we are
	public static int getTurnPassed() {
		return turnsPassed;
	}

	//get and set the turn updated bool
	public static bool getTurnUpdated() {
		return turnUpdated;
	}

	public static void setTurnUpdated(bool value) {
		turnUpdated = value;
	}
}