﻿using UnityEngine;

using System.Collections;

using UnityEngine.Networking;

using UnityEngine.UI;




public class PlayerController : NetworkBehaviour

{

	[SyncVar]

	public Color cubeColor;

	[SyncVar]

	private GameObject objectID;

	//determines which player's turn it is

	[SyncVar]

	public int turn;

	//variables for the game

	[SyncVar]
	public int money;

	[SyncVar]
	public int inventory;

	[SyncVar]
	public int backlog;

	[SyncVar(hook ="OnNumberChange" )]
	public int inputTextValue;

	[SyncVar]
	bool turnIsUpdated;

	[SyncVar]
	int delivery;

	[SyncVar]
	int order;

	//determines which player the current player is

	public uint player;

	private NetworkIdentity objNetId;

	Text playerText;

	Text identityText;

	Text inputText;

	Text numberText;

	Text moneyText;

	Text inventoryText;

	Text backlogText;

	Text turnText;

	Text gameName;

	void OnDestroy()

	{

		GameStateManager.removePlayer((int)player);

	}




	void Start()
	{
		//The network manager assigns network identities dynamically, 

		//so just use this value as the current player's id

		player =  this.GetComponent<NetworkIdentity>().netId.Value;

		//add player id to network
		GameStateManager.addPlayer((int)player);

		//get the current turn
		turn = GameStateManager.getPlayerTurn();


		//initialize player text, tells who's turn it is

		playerText = GameObject.FindGameObjectWithTag("PlayerTurnText").GetComponent<Text>();

		//tells you which player you are

		identityText = GameObject.FindGameObjectWithTag("IdentityText").GetComponent<Text>();

		if (isLocalPlayer) {
		identityText.text = "Player " + this.player;
		}

		//inputText = GameObject.FindGameObjectWithTag("InputText").GetComponent<Text>();
		inputText = GameObject.Find ("InputText").GetComponent<Text>();

		numberText = GameObject.Find ("NumberText").GetComponent<Text>();

		moneyText = GameObject.Find ("MoneyText").GetComponent<Text>();

		inventoryText = GameObject.Find ("InventoryText").GetComponent<Text>();

		backlogText = GameObject.Find ("BacklogText").GetComponent<Text>();

		turnText = GameObject.Find ("TurnText").GetComponent<Text>();

		//set starting values for the player
		money = 200;
		inventory = 20;

		gameName = GameObject.Find ("GameNameText").GetComponent<Text>();
		gameName.text = GameObject.Find ("GlobalControl").GetComponent<GlobalControl>().gameName;
		//Set all text to initial state
		if (isLocalPlayer) {
			changeDataText ();
		}
	}




	// Update is called once per frame

	void Update()

	{

		if (isLocalPlayer) {

			//checks to see if the player clicked

			CheckIfClicked();

			//makes sure that the player turn text is synced

			CmdGetTurn();

			changeTurnText();

			CmdUpdateTurn ();
			//Updates once per turn
			if (turn == player) {
				if (!turnIsUpdated) {
					GameUpdates ();
				}
			}
		}

	}

	//Update all game related variabels on the start of the turn, only for the player whos turn it is and only once.
	void GameUpdates() {
		CmdUpdatedTurnSet (true);
		turnIsUpdated = true;
		Debug.Log ("Once per turn !!");

		//punish the player for inventory and backlog
		money -= inventory * GameStateManager.inventoryCost;
		money -= backlog * GameStateManager.backlogCost;

		//Recieve an incoming delivery
		CmdGetDelivery();
		inventory += delivery;

		//check inventory vs incoming order and send what you can and add stuff to backlog if needed
		CmdRecieveOrder();

		if (order + backlog > inventory) {
			//Send everything we have, because we know we dont have enough.
			CmdSendShipment(inventory);
			//Set backlog to the correct new amount
			backlog = (order + backlog) - inventory;
			//Set inventory to 0
			inventory = 0;

			numberText.text = "You have not filled the order and now have a backlog of: " + backlog;
		} else {
			//Send the right amount
			int tempvalue = order + backlog;
			CmdSendShipment(tempvalue);
			//Set the right inventory
			inventory = inventory - (backlog + order);
			numberText.text = "You have filled the order of " + order + "!";
			//Set backlog right
			backlog = 0;
		}
		//refresh all the text
		changeDataText();
	}


	void OnNumberChange(int test) {
		//numberText.text = "You have recieved " + test + " units, and these have been added to your inventory!";
	}



	void CheckIfClicked()

	{

		if (Input.GetKeyDown(KeyCode.Return))
		{
			Debug.Log ("This happens");
			//update the variable

			CmdGetTurn();

			//check if it's my turn, if not, exit out

			if (turn != player) return;
			CmdChangeTurn();

			//change the color

			CmdGetInput ();

			objectID = GameObject.FindGameObjectsWithTag("Tower")[0];

			cubeColor = new Color(Random.value, Random.value, Random.value, Random.value);

			CmdChangeColor(objectID, cubeColor);

			//Change the bool in the gamestatemanager, to get ready for next player

			CmdUpdatedTurnSet (false);

		}

	}



	//updates state on the server

	[Command]

	void CmdChangeTurn()

	{

		if (turn == player) GameStateManager.nextTurn();

		turn = GameStateManager.getPlayerTurn();

		//syncs this all clients connected on server 

		RpcUpdateTurn(turn);

	}




	/*

    * Changes the turn text of the player

    */

	void changeTurnText()

	{

		if (turn == player) 
			{
			playerText.text = "Your turn. ";
			playerText.color = Color.green;
			}
			else if (turn != player) 
			{
			playerText.text = "Wait for your turn. ";
			playerText.color = Color.red;
			}
	}

	/*

    * Changes the players money, inventory and backlog;

    */

	void changeDataText()

	{

		if (!isLocalPlayer) {
			return;
		}
		moneyText.text = "Money: " + money;
		inventoryText.text = "Inventory: " + inventory;
		backlogText.text = "Backlog: " + backlog;
		turnText.text = "Turn: " + GameStateManager.getTurnPassed ();

	}


	[ClientRpc]

	void RpcUpdateTurn(int i)

	{

		turn = i;

		changeTurnText();

		changeDataText();

	}


	[Command]

	void CmdGetTurn()

	{

		turn = GameStateManager.getPlayerTurn();    

	}




	[Command]

	void CmdChangeColor(GameObject go, Color c)

	{

		objNetId = go.GetComponent<NetworkIdentity>();

		objNetId.AssignClientAuthority(connectionToClient);

		RpcUpdateTower(go, c);

		objNetId.RemoveClientAuthority(connectionToClient);

	}

	[Command]	
	void CmdGetInput() {
		objNetId = GameObject.Find ("InputField").GetComponent<NetworkIdentity>();
		objNetId.AssignClientAuthority(connectionToClient);
		inputText = GameObject.Find ("InputText").GetComponent<Text>();
		inputTextValue =  int.Parse(inputText.text);
		objNetId.RemoveClientAuthority(connectionToClient);

		//Send the order to the previous part of the chain.
		GameStateManager.SendOrder (inputTextValue);
	}

	/*

     * Being sent from server to

     */

	[ClientRpc]

	void RpcUpdateTower(GameObject go, Color c)

	{

		go.GetComponent<Renderer>().material.color = c;

	}   

	[Command]
	void CmdUpdateTurn(){
		turnIsUpdated = GameStateManager.getTurnUpdated ();
	}

	[Command]
	void CmdUpdatedTurnSet(bool value){
		GameStateManager.setTurnUpdated (value);
	}

	[Command]
	void CmdGetDelivery() {
		delivery = GameStateManager.GetDelivery ();
	}

	[Command]
	void CmdRecieveOrder() {
		order = GameStateManager.RecieveOrder();
	}

	[Command]
	void CmdSendShipment(int delivery) {
		GameStateManager.AddDelivery (delivery);
	}
}