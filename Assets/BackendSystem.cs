﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Experimental.Networking;
using UnityEngine.UI;

namespace AssemblyCSharp
{
	public class BackendSystem:MonoBehaviour
	{
		public GameObject g;

		void Start () {
			Debug.Log("NÅR SKJER DETTE");
			StartCoroutine(GetText());
		}

		IEnumerator GetText()
		{

			using (UnityWebRequest www = UnityWebRequest.Get("http://api.simulering.no/api/game/10"))
			{
				yield return www.Send();
				Debug.Log("GetText OK Send");
				if (www.isError)
				{
					Debug.Log(www.error);
				}
				else
				{
					Debug.Log("GetText OK");
					MyGameInfo mgi= JsonUtility.FromJson<MyGameInfo>(www.downloadHandler.text);
					// Show results as text
					//SetPlayerUidText(mgi.GamePin.ToString());
					//GameName.text=mgi.Name;
					g.GetComponent<GlobalControl>().gameName = mgi.Name;
					Debug.Log (mgi.Name);
					// Or retrieve results as binary data
					//byte[] results = www.downloadHandler.data;
				}

			}
		}
		IEnumerator Upload()
		{
			WWWForm form = new WWWForm();
			form.AddField("myField", "myData");

			using (UnityWebRequest www = UnityWebRequest.Post("http://api.simulering.no/api/values", form))
			{
				yield return www.Send();

				if (www.isError)
				{
					//SetPlayersCount(www.error);
				}
				else
				{
					//Alles OK
					//SetPlayersCount("Form upload complete!");
				}
			}
		}
	}
}

