﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Experimental.Networking;
using UnityEngine.UI;

public class GlobalControl : NetworkBehaviour
{
	public string gameName;

	public static GlobalControl Instance;


	void Awake()
	{

		if (Instance == null)
		{
			DontDestroyOnLoad(gameObject);
			Instance = this;
		}
		else if (Instance != this)
		{
			Destroy(gameObject);
		}
	}
}