﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CameraScript : NetworkBehaviour {

	public GameObject thePlayer;
	public Vector3 offset;

	// Use this for initialization
	void Start () {
		if (!isLocalPlayer) {
			return;
		}
		transform.position = new Vector3 (thePlayer.transform.position.x + offset.x, thePlayer.transform.position.y + offset.y, offset.z);
	}
	
	// Update is called once per frame
	void Update () {
		if (!isLocalPlayer) {
			return;
		}

	}
}
